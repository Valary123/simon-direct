var basePath = location.hostname;

if ( basePath.indexOf('localhost') > -1 ) {
  basePath = 'localhost';
}

var constants = {
  apiInitData: './all.json?v=' + Date.now(),
  apiSendUrlToClient: '//' + basePath + '/simondirect-api/api/sendUrlToClient',
  apiCompleteByYourself: '//' + basePath + '/simondirect-api/api/completeByYourself',
  apiBrokerAppointment: '//' + basePath + '/simondirect-api/api/brokerAppointment',
  apiForgetBrokerId: '//' + basePath + '/simondirect-api/api/forgetBrokerId',
  apiGetCobsByState: '//' + basePath + '/simondirect-api/api/cobsByState/',
  apiGetTeams: '//' + basePath + '/simondirect-api/api/teams/',
  apiSendContactUs: '//' + basePath + '/simondirect-api/api/sendContactUs/',
  hiscoxPartnerURL: '//quote.hiscox.com/portalserver/partner-agent/simonagency/quote-and-bind#',
  responseNoBroker: 'no broker',
  responseEmailSended: 'email sended'
};

var storage = {
  states: {},
  cobs: [],
  matrix: [],
  risks: {},
  hiscoxProducts: {},
  simonDocs: {},
  brokers: [],
  chosenCOB: '',
  chosenState: '',
  businessName: '',
  chosenProducts: [],
  chosenQuoteType: null,
  completeYourself: false,
  hiscoxSubcats: [],
  availableProducts: []
};

function startAnimateButton(btn) {
  btn.removeClass('animate');
  btn.addClass('animate');
};

function endAnimateButton(btn, btnclass){
  setTimeout(function(){
    btn.addClass(btnclass);
  },2500);

  setTimeout(function(){
    btn.removeClass('animate');
    btn.removeClass(btnclass);
  },6000);
};

function openModal(e, id){
  if ( e ) {
    e.preventDefault();
  }
  $('#'+id).arcticmodal({
    afterClose: function(data, el) {
      $('body').css('overflow-y','auto');
    }
  });
}

function initForgetBrokerId() {
  $('#forgetBrokerIdBtn').on('click', function () {
    const submitBtn = $(this);
    submitBtn.removeClass('success error animate');

    startAnimateButton(submitBtn);

    if ($(this).closest('form').valid()) {
      var data = {
        brokerEmail: $('[name="reset_broker_id"]').val() || ''
      };

      // disabling submit button
      var submitVal = submitBtn.val();
      submitBtn.attr("disabled", true);

      $.ajax({
        type: "POST",
        url: constants.apiForgetBrokerId,
        data: data
      })
        .done(function(res) {
          if ( constants.responseNoBroker === res ) {
            $('.modal__close').click();
            openModal(null, 'wrongBrokerEmail');
            submitBtn.removeClass('success error animate');
          }
          else if ( constants.responseEmailSended === res ) {
            $('[name="reset_broker_id"]').val('');
            $('.modal__close').click();
            openModal(null, 'checkYourEmail');
            submitBtn.removeClass('success error animate');
          }
        })
        .fail(function() {
          openModal(null, 'errorOccured');
          endAnimateButton(submitBtn,'errors');
        })
        .always(function(){
          // enabling submit button
          $('#forgetBrokerIdBtn').val(submitVal);
          $('#forgetBrokerIdBtn').removeAttr("disabled");
        });
    }
    else{
      endAnimateButton(submitBtn,'errors');
    }
  })
}

function processQuote() {
  if ( !storage.chosenState || !storage.chosenCOB ) {
    return;
  }
  const submitBtn = $('#form-quote button[type="submit"]');

  // disabling submit button
  var submitVal = submitBtn.val();
  submitBtn.attr("disabled", true);

  storage.businessName = $('[name="business_name"]').val() || '';
  if ( storage.chosenQuoteType == 2 ) {
    storage.chosenCOB = $('#industries').val() || '';
  }

  // send to client
  if ( !storage.completeYourself ) {

    var data = {
      brokerIdOrEmail: $('[name="broker_id"]').val() || '',
      clientEmail: $('#client_email').val() || '',
      chosenCOB: storage.chosenCOB,
      businessName: storage.businessName,
      chosenState: storage.chosenState
    };

    $.ajax({
      type: "POST",
      url: constants.apiSendUrlToClient,
      data: data
    })
      .done(function(res) {
        if ( constants.responseNoBroker === res ) {
          endAnimateButton(submitBtn,'errors');
          openModal(null, 'modal-forgot');
        }
        else if ( constants.responseEmailSended === res ) {
          endAnimateButton(submitBtn,'success');
          openModal(null, 'processQuoteSendToClient');
          $('[name="broker_id"]').val('');
          $('#client_email').val('');
          $('#client_confirm_email').val('');
          $('[name="business_name"]').val('');
          $('#industries').prop('selectedIndex',0)
          .trigger('refresh');
        }
      })
      .fail(function() {
        openModal(null, 'errorOccured');
        endAnimateButton(submitBtn,'errors');
      })
      .always(function(){
        // enabling submit button
        submitBtn.removeAttr("disabled");
      });
  }
  // complete yourself
  else {

    var data = {
      brokerIdOrEmail: $('[name="broker_id"]').val() || '',
      chosenCOB: storage.chosenCOB,
      chosenState: storage.chosenState,
      chosenQuoteType: storage.chosenQuoteType,
      chosenProducts: storage.chosenProducts
    };

    // instant issue quote - 2
    if ( storage.chosenQuoteType == 2 ) {
      $.ajax({
        type: "POST",
        url: constants.apiCompleteByYourself,
        data: data
      })
        .done(function(res) {
          if ( constants.responseNoBroker === res ) {
            openModal(null, 'modal-forgot');
            endAnimateButton(submitBtn,'errors');
          }
          // json string
          else { // if ( res.indexOf("{") > -1 ) {

            // parsing JSON
            var broker = {};
            try {
              broker = res; //JSON.parse(res);
            } catch(e) {
              console.log(e);
              openModal(null, 'errorOccured');
              endAnimateButton(submitBtn,'errors');
            }

            // forming up params
            var urlParams =
              '?restoreforms=false'
              + '&agentName=' + broker.name
              + '&agencyName=' + broker.agencyName
              + '&agentId=' + broker.id
              + '&phoneNo=' + broker.phone
              + '&agentEmail=' + broker.email
              + '&businessName=' + storage.businessName
              + '&state=' + storage.chosenState
              + '&cob=' + storage.chosenCOB
            ;
            urlParams = encodeURI(urlParams);

            $('#modalBrokerName').html(broker.name);
            $('#modalLink').html(
              '<a href="' + constants.hiscoxPartnerURL + urlParams + '" target="_blank">CONFIRM</a>'
            );
            openModal(null, 'instantQuoteLink');
            endAnimateButton(submitBtn,'success');

            $('[name="broker_id"]').val('');
            $('#client_email').val('');
            $('#client_confirm_email').val('');
            $('[name="business_name"]').val('');
            $('#industries').prop('selectedIndex',0)
            .trigger('refresh');
          }
        })
        .fail(function() {
          openModal(null, 'errorOccured');
          endAnimateButton(submitBtn,'errors');
        })
        .always(function(){
          // enabling submit button
          submitBtn.removeAttr("disabled");
        });
    }
    // shop it around - 3
    else if ( storage.chosenQuoteType == 3 ) {

      // calc docs to be send
      var docs = [];
      // docs by cob
      docs = storage.simonDocs['cobs'][storage.chosenCOB];
      // docs by products
      storage.chosenProducts.forEach(function(product){
        docs = docs.concat(storage.simonDocs['products'][product]);
      });
      // unique array
      docs = docs.filter(function(value, index, self){
        return self.indexOf(value) === index;
      });
      // adding docs to send data
      data.docs = docs;

      $.ajax({
        type: "POST",
        url: constants.apiCompleteByYourself,
        data: data
      })
        .done(function(res) {
          if ( constants.responseNoBroker === res ) {
            openModal(null, 'modal-forgot');
            endAnimateButton(submitBtn,'errors');
          }
          else if ( constants.responseEmailSended === res ) {
            openModal(null, 'processQuoteCompleteYourSelf');
            endAnimateButton(submitBtn,'success');
            $('[name="broker_id"]').val('');
            $('#client_email').val('');
            $('#client_confirm_email').val('');
            $('[name="business_name"]').val('');
          }
        })
        .fail(function() {
          openModal(null, 'errorOccured');
          endAnimateButton(submitBtn,'errors');
        })
        .always(function(){
          // enabling submit button
          submitBtn.removeAttr("disabled");
        });
    }
  }
}

function resetSteps() {
  $('#step_2, #step_3, #step_4').hide();
  $('#step_2_industry').html('');
  $('#step_2_state').html('');
  $('#step_2_products').html('');
}

function logicStep3() {
  $('#btnStep2').on('click', function () {
    // store checked products
    storage.chosenProducts = [];
    $('.checkbox--square-full input').each(function (i, el) {
      if ( $(el).is(':checked') ) {
        storage.chosenProducts.push($(el).val());
      }
    });

    if ( storage.chosenQuoteType == 2 ) {
      $('#industries-styler').show();
    }
    else {
      $('#industries-styler').hide();
    }
  });
}

function logicStep4() {
  $('.js-step4-to-client').on('click', function () {
    $.arcticmodal('close');
    $('#step4_to_client .js-choose-sender').click();
  });
  $('.js-step4-yourself').on('click', function () {
    $.arcticmodal('close');
    $('#step4_yourself .js-choose-sender').click();
  });


  $('#step4_to_client').on('click', function () {
    storage.completeYourself = false;
    $('#client_email, #client_confirm_email').show();
  });
  $('#step4_yourself').on('click', function () {
    storage.completeYourself = true;
    $('#client_email, #client_confirm_email').hide();
  });

  $('body').on('submit','.form--quote',function(e){
    if ($(this).valid()){
      $(this).find('.step--submit').removeClass('current').addClass('done');
    }

    if ( storage.chosenQuoteType == 2 ) {
      if ($('#industries').val() == $('#industries').find("option:first-child").val()) {
        $('#selectIndustry4').arcticmodal();
        const submitBtn = $(this).find('.js-quote-submit');
        startAnimateButton(submitBtn);
        endAnimateButton(submitBtn,'errors');
        return;
      }
    }

    if ($('form.form--quote').valid()) {
      processQuote();
    }
  });
}

function formStep2() {

  // filter companies match step 1 conditions
  var companiesMatch = _.filter(storage.matrix, function(company){
    return (company.states.indexOf(storage.chosenState) > -1) && (company.industies.indexOf(storage.chosenCOB) > -1);
  });

  // # option "Quote Online Now"
  // disable all companies and enable matched companies

  $('.company').addClass('disabled');  //
  companiesMatch.forEach(function (company) {
    $('[id="' + company.name + '"]').removeClass('disabled');
  });
  // disable the option if no avail companies
  var availCompaniesCount = 0;
  $('.company').each(function(i, el){
    if ( !$(el).hasClass('disabled') ) {
      availCompaniesCount++;
    }
  });

  let optionId = 1; 
  if ( !availCompaniesCount ) {
    optionId = 2;
    $('#step_2_option_1').addClass('disabled');
  }
  else {
    optionId = 1;
    $('#step_2_option_1').removeClass('disabled');
  }

  // # option "Explore an Instant Issue Quote"
  // disable an option, if no hiscox for conditions
  // and go to "Let’s Shop it Around Quickly"
  if ( !storage.hiscoxProducts[storage.chosenCOB] ) {
    $('#step_2_option_2').addClass('disabled');
    if(optionId == 2) {
      optionId = 3;
    }
  }
  else {
    $('#step_2_option_2').removeClass('disabled');    
    $('#step_2_industry').html(storage.chosenCOB);
    $('#step_2_state').html(storage.chosenState);
    
    let products = [];
    storage.availableProducts.forEach(function(comavailableProductpany) {
      if(storage.hiscoxProducts[storage.chosenCOB][comavailableProductpany] && storage.hiscoxProducts[storage.chosenCOB][comavailableProductpany].indexOf( storage.chosenState ) !== -1) {
        products.push(comavailableProductpany);
      }
    });

    if(products.length === 0) {
      if(optionId == 2) {
        optionId = 3;
      }
      $('#step_2_option_2').addClass('disabled');
    }
    else {
      $('#step_2_products').html(products.join(', '));
    }
  }
  $('#step_2_option_' + optionId + ' input').click();

  // # option "Let’s Shop it Around Quickly"
  // disable all products and remove check
  $('.checkbox--square-full').addClass('disabled');
  $('.checkbox--square-full input').prop('checked', false);
  // enable matched products
  var isProducts = false;
  companiesMatch.forEach(function(company) {
    company.products.forEach(function(productName) {
      isProducts = true;
      $('input[value="' + productName + '"]').parent().parent().removeClass('disabled');
    });
  });

  if(isProducts) {
    $('#step_2_option_3').removeClass('disabled');
  }
  else {
    $('#step_2_option_3').addClass('disabled');
  }
}

function onSelectItemStep1(chosenCob, isFirst) {
  // hide next steps
  $('#step_2, #step_3, #step_4').hide();

  if ( !chosenCob ) {
    return;
  }
  else {
    storage.chosenCOB = chosenCob;
  }

  // Note: David suggested substituting "Professional Liability" with a second product
  if(storage.chosenCOB === "Garage & Dealer") {
    $('.product-professional-liability').hide();
    $('.product-garage-keepers').show();
  }
  else {
    $('.product-garage-keepers').hide();
    $('.product-professional-liability').show();
  }

  // companies with choosen cob
  var availCompanies = _.filter(storage.matrix, function(o){
    return o.industies.indexOf(chosenCob) > -1;
  });

  // get states from companies above
  var availStates = [];
  availCompanies.forEach(function (company) {
    availStates = company.states.concat(availStates);
  });
  availStates = _.uniq(availStates);

  var optionsStates = '<option class="disabled">Choose State</option>';
  for ( var state in storage.states ) {
    if ( availStates.indexOf(storage.states[state]) > -1 ) {
      optionsStates += '<option value="' + storage.states[state] + '">' + state + '</option>';
    }
  }
  $('#states').html(optionsStates);

  // reinit select
  $('select')
    .styler('destroy')
    .styler()
    .styler({
      selectSearch: true,
      locale: 'en',
      locales: {
        'en': {
          filePlaceholder: 'No file selected',
          fileBrowse: 'Browse...',
          fileNumber: 'Selected files: %s',
          selectPlaceholder: 'Select...',
          selectSearchNotFound: 'No matches found',
          selectSearchPlaceholder: 'Search...'
        }
      }
    });

  // set risks
  var risks = storage.risks.Simon[storage.chosenCOB] || storage.risks.Hiscox[storage.chosenCOB];
  var risksHTML = '';
  risks.forEach(function (risk) {
    risksHTML += '<li>'  + risk + '</li>';
  });
  $('[class="risks"]').html(risksHTML);

  $('#states').prop('selectedIndex',0).trigger('refresh');
  if ($(window).width() < 480 && !isFirst) {
    $("#risksWeCover").arcticmodal();
  }

  $('#scroll-disable').mCustomScrollbar("destroy");
  $('#scroll-disable').mCustomScrollbar({
    theme:"rounded",
    scrollbarPosition:'outside',
    scrollButtons:{
      enable: true
    },
    mouseWheel:{ preventDefault: true },
    callbacks:{
      onOverflowY:function(){
        $('#industry-angle-down').show();
      },
      onOverflowYNone:function(){
        $('#industry-angle-down').hide();
      },
      onTotalScroll:function(){
        $('#industry-angle-down').hide();
      },
      onTotalScrollOffset: 30
    }
  });
}

function populateAndLogicStep1() {
  $.getJSON(constants.apiInitData, function (data) {
    // states
    data.states.forEach(function (state) {
      storage.states[state.name] = state.code;
    });
    // cobs
    storage.cobs = data.cobs;
    // matrix
    storage.matrix = data.matrix;
    // risks
    storage.risks = data.risks;
    // hiscox products
    storage.hiscoxProducts = data.hiscoxProducts;
    // simon docs
    storage.simonDocs = data.simonDocs;

    storage.availableProducts = data.availableProducts;

    // cobs populate
    var optionCobs = '';
    storage.cobs.forEach(function(cob, i){
      optionCobs +=
        '<div class="radio radio--square-full">' +
        '  <label>' +
        '    <input type="radio" name="industry" value="' + cob + '">' +
        '    <div class="radio__label label">' + cob + '</div>' +
        '  </label>' +
        '</div>';
    });
    $('#cobs').html(optionCobs);

    // states populate depending on cobs
    $('#cobs div').on('click touch', function(el) {
      var chosenCob = $(el.target)[0].value;
      $('.step1-submit-alarm .step__step1-alarm').hide();
      onSelectItemStep1(chosenCob, false);      
    });

    $('#states').on("change", function(el) {

      // reset next steps
      resetSteps();
      storage.chosenState = el.target.value;
      $('.step1-submit-alarm .step__step1-alarm').show();
      formStep2();
    });

    // choosing first industry
    $('#cobs label:first > input[type="radio"]').prop('checked', true);
    onSelectItemStep1($('#cobs label:first > div').text(), true);
    //$('#cobs label:first > div')[0].click();

    storage.hiscoxSubcats = data.hiscoxSubcats;
  });

  // store an quote type option
  $('input[name="quote_type"]').on('click', function(e) {
    if(($(e.target).val() == 2 ||  $(e.target).val() == 3) && storage.chosenState == '') {
      mainFunctions.animateTo($('#quoteSteps'));
      $('#step_2').fadeOut(300).removeClass('current');
      var chosenCob = $('#cobs div')[0].value;
      onSelectItemStep1(chosenCob, false);
    }
    storage.chosenQuoteType = $(e.target).val();    
  });
}

function initBrokerAppointment() {
  $.getJSON(constants.apiInitData, function (data) {
    // states
    var optionsStates = '<option value="" class="disabled">Choose State</option>';
    for ( var i in data.states ) {
      storage.states[data.states[i].name] = data.states[i].code;
      optionsStates += '<option value="' + data.states[i].name + '">' + data.states[i].name + '</option>';
    }  
    $('#appointment_states').html(optionsStates);
    $('#appointment_states').prop('selectedIndex',0)
      .trigger('refresh');
  });
  
  $('input[name="tel"]').mask('1 (999) 999-9999');
}

$(function(){
  populateAndLogicStep1();
  logicStep3();
  logicStep4();
  initForgetBrokerId();

  if($('#form-appointment').length) {
    initBrokerAppointment();
  }
});

var mainFunctions = (function () {
  'use strict';

  var h;

  init();

  return {
    animateTo: animateTo
  };

  function init() {
      $(activate);
  }

  function activate() {

    $(window).on('scroll', function(){
      $('#menu').slideUp(300).removeClass('active');
    });


    var $hiscoxLink = $('#js-hiscox');
    if($hiscoxLink.length) {
      var params = window.location.search.substring(1);
      $hiscoxLink.attr('href', constants.hiscoxPartnerURL + '?' + params);
    }

    if ($('.content').find('.js-hfixed').length > 0) {
      h = $('.js-hfixed').offset().top;
    } else {h = 0;}

    /*
        $('input[type=radio][name=industry]').change(function() {
          $('#scroll-disable').removeClass('disabled');
          $('#states').prop('selectedIndex',0)
          .trigger('refresh');
          if ($(window).width() < 480) {
            $("#risksWeCover").arcticmodal();
          }
        });
    */

    if ($('.content').find('.js-hfixed').length > 0) {
      $(window).on('scroll', function() {
        var $target = $('.header');
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        var $target_clone = $('.header_fixed_clone');
        if (scrollTop >= $target_clone.outerHeight()) {
          $target.addClass('header_fixed');
        } else {
          $target.removeClass('header_fixed');
        }
      });
    }

    $('#states').on('change', function() {
      if (!$('input[name="industry"][type="radio"]:checked').length) {
        $('#states').prop('selectedIndex',0)
        .trigger('refresh');
        $('#selectIndustry').arcticmodal();

      }
    });

    setInterval(pulseGo, 2000);

    $('body').on('click','.graph-control__list', function(e){
      switch (e.target.id) {
        case 'graph-control1':
          $('#graph-indicator2').css('height', '40%');
          $('#graph-indicator3').css('height', '70%');
          $('#graph-indicator4').css('height', '50%');
          $('#graph-indicator5').css('height', '70%');
          $('#graph-indicator6').css('height', '85%');
          break;
        case 'graph-control2':
          $('#graph-indicator2').css('height', '35%');
          $('#graph-indicator3').css('height', '40%');
          $('#graph-indicator4').css('height', '30%');
          $('#graph-indicator5').css('height', '30%');
          $('#graph-indicator6').css('height', '55%');
          break;
        case 'graph-control3':
          $('#graph-indicator2').css('height', '65%');
          $('#graph-indicator3').css('height', '60%');
          $('#graph-indicator4').css('height', '70%');
          $('#graph-indicator5').css('height', '60%');
          $('#graph-indicator6').css('height', '70%');
          break;
        case 'graph-control4':
          $('#graph-indicator2').css('height', '30%');
          $('#graph-indicator3').css('height', '65%');
          $('#graph-indicator4').css('height', '40%');
          $('#graph-indicator5').css('height', '85%');
          $('#graph-indicator6').css('height', '60%');
          break;
        case 'graph-control5':
          $('#graph-indicator2').css('height', '70%');
          $('#graph-indicator3').css('height', '30%');
          $('#graph-indicator4').css('height', '60%');
          $('#graph-indicator5').css('height', '55%');
          $('#graph-indicator6').css('height', '80%');
          break;
        case 'graph-control6':
          $('#graph-indicator2').css('height', '5%');
          $('#graph-indicator3').css('height', '55%');
          $('#graph-indicator4').css('height', '10%');
          $('#graph-indicator5').css('height', '75%');
          $('#graph-indicator6').css('height', '20%');
          break;
        case 'graph-control7':
          $('#graph-indicator2').css('height', '50%');
          $('#graph-indicator3').css('height', '45%');
          $('#graph-indicator4').css('height', '7%');
          $('#graph-indicator5').css('height', '20%');
          $('#graph-indicator6').css('height', '10%');
          break;
        case 'graph-control8':
          $('#graph-indicator2').css('height', '2%');
          $('#graph-indicator3').css('height', '5%');
          $('#graph-indicator4').css('height', '2%');
          $('#graph-indicator5').css('height', '25%');
          $('#graph-indicator6').css('height', '25%');
          break;
        case 'graph-control9':
          $('#graph-indicator2').css('height', '14%');
          $('#graph-indicator3').css('height', '8%');
          $('#graph-indicator4').css('height', '35%');
          $('#graph-indicator5').css('height', '10%');
          $('#graph-indicator6').css('height', '15%');
          break;
        case 'graph-control10':
          $('#graph-indicator2').css('height', '24%');
          $('#graph-indicator3').css('height', '50%');
          $('#graph-indicator4').css('height', '20%');
          $('#graph-indicator5').css('height', '40%');
          $('#graph-indicator6').css('height', '50%');
          break;
        case 'graph-control11':
          $('#graph-indicator2').css('height', '10%');
          $('#graph-indicator3').css('height', '13%');
          $('#graph-indicator4').css('height', '15%');
          $('#graph-indicator5').css('height', '15%');
          $('#graph-indicator6').css('height', '3%');
          break;
        case 'graph-control12':
          $('#graph-indicator2').css('height', '80%');
          $('#graph-indicator3').css('height', '75%');
          $('#graph-indicator4').css('height', '65%');
          $('#graph-indicator5').css('height', '65%');
          $('#graph-indicator6').css('height', '65%');
          break;
      }
      if ($(window).width() < 480) {
        animateTo($(".why__right"));
        $(".why__right .btn-wrap").css('display', 'block');
      } else $(".why__right .btn-wrap").css('display', 'none');
    });

    $('.carousel').each(function() {
      $(this).slick({
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        swipeToSlide: true,
        speed: 500,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000,
        nextArrow: '<span class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
        prevArrow: '<span class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });

    });

    $('*[class*="js-toggle-"]').each(function(){
      $(this).on('click',function(e){
        var base = 'js-toggle-';
        var cl = $.grep(this.className.split(" "), function(v, i){
            return v.indexOf(base) === 0;
        }).join();
        var target = cl.substring(base.length);
        toggleBlock(e,target);
      });
    });

    var vids = $("video");
      $.each(vids, function(){
           this.controls = false;
    });

    $("video,.js-play-video").click(function() {
      var $this = ($(this).hasClass('js-play-video')) ? $(this).siblings('.video')[0] : this;
      if ($this.paused) {
        $this.play();
        $($this).siblings('.js-play-video').fadeOut();
        $this.controls = true;
      } else {
        $this.pause();
      }
    });

    $('body').on('click','.modal__close', function(){
      $.arcticmodal('close');
    });

    $('body').on('click','.js-open-menu', function(e){
      e.preventDefault();
      toggleMenu();
    });

    var tap = (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) ? "touchstart" : "click";
    $(document).on(tap, function (event) {
      if ($(event.target).closest("#menu").length || $(event.target).closest(".js-open-menu").length) {
        return;
      }
      if ($("#menu").hasClass("active")) {
        toggleMenu();
        event.stopPropagation();
      }
    });

    $(document).keydown(function(e) {
        if (e.keyCode === 27 && $("#menu").hasClass("active")) {
          e.preventDefault();
          toggleMenu();
        }
    });

    $('body').on('click','.overlay', function(){
      toggleMenu();
      $(this).fadeToggle(300);
    });

    window.onload = function() {
    if ( document.location.href.indexOf('#') > -1 ) {
      var href = document.location.href.substring(document.location.href.indexOf('#'));
      if (href.indexOf('/')!=-1){
          href = document.location.href.substring(document.location.href.indexOf('#')+2);
      }
      else{
          href = document.location.href.substring(document.location.href.indexOf('#')+1);
      }
      $('html, body').animate({
          
          scrollTop: $('#'+href).offset().top - $('header').height()
      }, 1000);
      }
    }

    var headerClone = $('header').clone().appendTo('body');
    headerClone.addClass('header_fixed header_fixed_clone');    
    var height_header_fx = headerClone.outerHeight();
    $('.header_fixed_clone').css('display', 'none');

    $('a[href*="#"]:not([href="#"])').click(function(e) {      
      var $this = this;

      if ($(this).closest('#menu').length){
        toggleMenu();
      }
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash.replace('/',''));
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        var headerTop = (target.offset().top > h) ? height_header_fx : 70;
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top - headerTop
          }, 1000, function() {
            if($($this).hasClass('js-btn-animate')) {
              $('.btn--circle').addClass('scale--btn')
              setTimeout(function() {$('.btn--circle').removeClass('scale--btn')}, 500)
            };
          });
          return false;
        }
      }
    });

    $.validator.addMethod('filesize', function (value, element, param) {
      return this.optional(element) || (element.files[0].size <= param)
    }, $.validator.format( "The uploading file is too large (max 10 mb)." ) );

    $.validator.addMethod( "extension", function( value, element, param ) {
      param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
      return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
    }, $.validator.format( "Invalid format: Pdf, Jpg, Png, Docx, Doc, Xlsx, Xls files only." ) );

    // broker appointment form
    $('#form-appointment').validate({
      rules: {
        hiddenRecaptcha: {
          required: function () {
              if (grecaptcha.getResponse() == '') {
                  return true;
              } else {
                  return false;
              }
          }
        },
        tel: {          
          required: true
        },
        state: {          
          required: true
        },
        f_license: {
          filesize: 10485760,
          extension: "gif|jpeg|jpg|png|bmp|pdf|doc|dot|docx|dotx|docm|dotm|xls|xlt|xla|xlsx|xltx|xlsm|xltm|xlam|xlsb"
        },
        f_eo: {
          filesize: 10485760,
          extension: "gif|jpeg|jpg|png|bmp|pdf|doc|dot|docx|dotx|docm|dotm|xls|xlt|xla|xlsx|xltx|xlsm|xltm|xlam|xlsb"
        },
        f_w9: {
          filesize: 10485760,
          extension: "gif|jpeg|jpg|png|bmp|pdf|doc|dot|docx|dotx|docm|dotm|xls|xlt|xla|xlsx|xltx|xlsm|xltm|xlam|xlsb"
        },
        f_agreement: {
          filesize: 10485760,
          extension: "gif|jpeg|jpg|png|bmp|pdf|doc|dot|docx|dotx|docm|dotm|xls|xlt|xla|xlsx|xltx|xlsm|xltm|xlam|xlsb"
        }
      },
    });

    function onSendEmail(e) {
      e.preventDefault();

      var captchaid = $(this).closest('form').find('.g-recaptcha').attr('id');
      var response = grecaptcha.getResponse();
      var valid = false;
      if (response != '') {
          $(this).closest('form').find('.form__step:last').removeClass('error');
          valid = true;
      } else {
          $(this).closest('form').find('.form__step:last').addClass('error');
      }

      /*
      // validation for uploading files size
      var sizeValid = false;
      if ($('.upload__size-error').length==0) {
        sizeValid = true;
      }*/

      const submitBtn = $(this);
      submitBtn.removeClass('success errors animate');
      startAnimateButton(submitBtn);

      if ($(this).closest('form').valid() && valid) {
        var formData = new FormData();
        // serialize form fields "Here is what we need from you"
        formData.append("f_name", $('[name="f_name"]').val());
        formData.append("l_name", $('[name="l_name"]').val());
        formData.append("c_name", $('[name="c_name"]').val());
        formData.append("email", $('[name="email"]').val());
        formData.append("tel", $('[name="tel"]').val());
        formData.append("m_email", $('[name="m_email"]').val());
        var city = `${$('[name="city"]').val()}, ${$('[name="state"]').val()}, ${$('[name="zip_code"]').val()}`;
        formData.append("city", city);
        formData.append("business", $('[name="business"]').val());
        // serialize form fields "Upload Required Documents"
        formData.append("sheduledate", $('[name="sheduledate"]').val());
        formData.append("sheduletimezone", $('#sheduletimezone').val());
        formData.append("sheduletime", $('[rname="sheduletime"]').val());
        // serialize form fields "Schedule a Time to Chat"
        formData.append("f_license", $('[name="f_license"]')[0].files[0]);
        formData.append("f_eo", $('[name="f_eo"]')[0].files[0]);
        formData.append("f_w9", $('[name="f_w9"]')[0].files[0]);
        formData.append("f_agreement", $('[name="f_agreement"]')[0].files[0]);
        formData.append("g-recaptcha-response", $('[name="g-recaptcha-response"]').val());

        // disable submit button
        var submitVal = $('#ba_submit').val();
        $('#ba_submit')
          .val('Processing ...')
          .attr("disabled", true);

        // send data to server
        $.ajax({
          url: constants.apiBrokerAppointment,
          data: formData,
          processData: false,
          contentType: false,
          type: 'POST',
        })
        .done(function(res){
          if ( res.status === 200 ) {
            endAnimateButton(submitBtn,'success');

            openModal(null, 'submitBrokerAppointment');

            $('#appointment_states').prop('selectedIndex',0).trigger('refresh');
            $('[name="f_name"]').val('');
            $('[name="l_name"]').val('');
            $('[name="c_name"]').val('');
            $('[name="email"]').val('');
            $('[name="tel"]').val('');
            $('[name="m_email"]').val('');
            $('[name="city"]').val('');
            $('[name="state"]').val('');
            $('[name="zip_code"]').val('');
            $('[name="business"]').val('');
            $('[name="f_license"]').val('');
            $('[name="f_eo"]').val('');
            $('[name="f_w9"]').val('');
            $('[name="f_agreement"]').val('');
            $('.upload.done').removeClass('done');
            $(window).scrollTop(0);

            submitBtn.addClass('disabled');
          }
          else {
            openModal(null, 'errorOccured');
            endAnimateButton(submitBtn,'errors');
          }
        })
        .fail(function() {
          openModal(null, 'errorOccured');
          endAnimateButton(submitBtn,'errors');
        })
        .always(function(){
          // enable submit button
          $('#ba_submit')
            .removeAttr("disabled");
            grecaptcha.reset();


            // After a successful submit, the page should be refreshed
           // window.location.reload();
           // $(window).scrollTop(0);
        });
      }
      else{
        endAnimateButton(submitBtn,'errors');
      }
    }

    $(document).on('click', '.js-form-submit', onSendEmail);

    $(document).on('click', '.js-quote-submit',function(e){
      const submitBtn = $(this);
      submitBtn.removeClass('success error animate');

      startAnimateButton(submitBtn);

      if (!$(this).closest('form').valid()){
        endAnimateButton(submitBtn,'errors');
      }
    });

    $.validator.addMethod("phone", function(value, element) {
      return this.optional(element) || /^[0-9\+]{1,}[0-9\-\(\)]{3,15}$/.test(value);
    });

    $('#form-quote').validate({
      //ignore: "",
      rules: {
        // client_confirm_email: {
        //   equalTo: '#client_email'
        // },
        'custom_rate': {
          required: true,
        },
        'industry': {
          required: true,
        },
        broker_phone: {
          phone: true
        },
      },
      errorPlacement: function(error, element) { },
      highlight: function(element, errorClass, validClass) {
        var elements = $(element);
        if (elements.attr('name') === 'custom_rate') {
            elements = $('input[name="custom_rate"]');
            elements = elements.add(elements.next());
        }
        elements.addClass(errorClass).removeClass(validClass);
        var elements2 = $(element);
        if (elements2.attr('name') === 'industry') {
            elements2 = $('input[name="industry"]').closest('.industry__select');
        }
        elements2.addClass(errorClass).removeClass(validClass);
      },

      unhighlight: function(element, errorClass, validClass) {
          var elements = $(element);
          if (elements.attr('name') === 'custom_rate') {
              elements = $('input[name="custom_rate"]');
              elements = elements.add(elements.next());
          }
          elements.removeClass(errorClass).addClass(validClass);
          var elements2 = $(element);
          if (elements2.attr('name') === 'industry') {
              elements2 = $('input[name="industry"]').closest('.industry__select');
          }
          elements2.removeClass(errorClass).addClass(validClass);
      },
      invalidHandler: function(e, validator) {
        var hIos = ($(validator.errorList[0].element).offset().top > 0) ? $(validator.errorList[0].element).offset().top : $(document).scrollTop();
        if(/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
          window.setTimeout(function() {
            $("html,body").animate({
              scrollTop: hIos
            });
          }, 0);
        }
      },
      // submitHandler: function onSubmit() {
      //   grecaptcha.execute();
      //   console.log('Submit');
      // },
    });

    $('.form').each(function() {
      $(this).validate();
    });

    $('.js-select').styler();

    $('.js-select-search').styler({
      selectSearch: true,
      locale: 'en',
      locales: {
        'en': {
          filePlaceholder: 'No file selected',
          fileBrowse: 'Browse...',
          fileNumber: 'Selected files: %s',
          selectPlaceholder: 'Select...',
          selectSearchNotFound: 'No matches found',
          selectSearchPlaceholder: 'Search...'
        }
      }
    });

    $('#scroll-leadership').mCustomScrollbar({
      theme:"rounded",
      scrollbarPosition:'outside',
      scrollButtons:{
        enable: true
      }
    });

    $('body').on('change','.quote-type__header input[type=radio]',function(){
      $(this).closest('.quote-type').find('.quote-type__item').removeClass('active');
      $(this).closest('.quote-type__item').addClass('active');
      $(this).closest('.quote-type__item').siblings().find('.quote-type__body input[type=checkbox]:checked').prop( "checked", false );
    });

    $('body').on('change','.quote-type__item:not(.active) .checkbox-list input[type=checkbox]',function(){
      $(this).closest('.quote-type__item').find('.quote-type__header input[type=radio]').click();
    });

    $('body').on('click','.js-choose-sender',function(e){
      nextStep($(this));
      var sender = $(this).attr('quote-send');
      $('.step.step--submit .quote-sender-ttl').text(sender);
      $('.step.step--submit #sender').val(sender);
      // if (sender == 'Complete it Here') {
      //   $('#client_email').css('display', 'none');
      //   $('#client_confirm_email').css('display', 'none');
      // }
      // if (sender == 'Send to Client') {
      //   $('#client_email').css('display', 'inline-block');
      //   $('#client_confirm_email').css('display', 'inline-block');
      // }
    });

    // $('body').on('submit','.form--quote',function(e){
    //   if ($(this).valid()){
    //     $(this).find('.step--submit').removeClass('current').addClass('done');
    //   }
    //   if ($('form.form--quote').valid()) {openModal(e, 'processQuote')}
    // });

    // $('body').on('click','.js-open-question',function(e){
    //   e.preventDefault();
    //   var text = $(this).find('.question__text').html();
    //   $('#modal-question').arcticmodal({
    //     afterClose: function(data, el) {
    //       $('body').css('overflow-y','auto');
    //     },
    //     beforeOpen: function(data, el) {
    //       $('#modal-question .question-text').html(text);
    //     },
    //   });
    // });

    $('body').on('click','.js-open-question',function (){
      $('html, body').animate({
        scrollTop: $('#why').offset().top - $('header').outerHeight()
      }, 1000);
    });

    $('body').on('click','#goToNo', function(e) {
      $('#no').click();
    });

    $('body').on('click','[data-filter]',function(e){
      var target = $(this).data('filter');
      $(this).addClass('active');
      $('[data-cat],[data-filter]').removeClass('active');
      $('[data-cat='+target+']').addClass('active');
      $(this).closest('.team').find('.vertical-scroll').mCustomScrollbar("scrollTo",$('[data-cat='+target+']').first().position().top);
    });

    $('body').on('click','.js-next-step',function(e){
      nextStep($(this));
    });

    /*$('body').on('click','.js-online-link',function(e){
      e.preventDefault();

      $('#cobs div')[0].click();
      $('#states').prop('selectedIndex',0).trigger('refresh');

      $('#step_2_option_1').removeClass('disabled');
      $('#step_2_option_2').removeClass('disabled');
      $('#step_2_option_3').removeClass('disabled');

      $('.choose-blocks .choose-blocks__item').hide();
      $('.choose-blocks .choose-blocks__item:first-child').show();

      $('[name="quote_type"][value="1"]').prop('checked', true);

      $('.companies-wrap .company.disabled').removeClass('disabled');
      storage.chosenState = '';

      var next = $('#step_2');    
      next.fadeIn(300).addClass('current');
      animateTo(next);
    });*/

    $('body').on('click','.js-open-video', function(e){
      openModal(e,'modal-video');
      $('video').trigger('click');
    });

    $('body').on('click','.js-open-sendclient',function(e){
      openModal(e,'modal-sender-client');
    });

    $('body').on('click','.js-open-appointment',function(e){
      toggleMenu();
      openModal(e,'modal-appointment');
    });

    $('body').on('click','.js-open-program',function(e){
      openModal(e,'modal-program');
    });

    $('body').on('click','.js-open-forgotid',function(e){
      openModal(e,'modal-forgot');
    });

    $('body').on('click','.js-open-companies',function(e){
      openModal(e,'modal-companies');
    });

    $('body').on('submit','.form--contact',function(e){
      if ($('form.form--contact').valid()) {
        const submitBtn = $('.js-send-contact');
        submitBtn.removeClass('success error animate');
  
        startAnimateButton(submitBtn);
        
        $.ajax({
          url: constants.apiSendContactUs,
          type: 'POST',
          data: $('form.form--contact').serializeArray(),    
          dataType: "json"
        })
        .done(function(res){
          if ( res.status === 200 ) {
            endAnimateButton(submitBtn,'success');

            openModal(null, 'contactUs');

            $('[name="contact_fname"]').val('');
            $('[name="contact_lname"]').val('');
            $('[name="contact_email"]').val('');
            $('[name="contact_phone"]').val('');
            $('[name="contact_comment"]').val('');
            $('[name="contact_robot"]').prop('checked', false);
          }
          else {
            endAnimateButton(submitBtn,'errors');
          }
        })
        .fail(function() {
          endAnimateButton(submitBtn,'errors');
        });
      }

      return false;
    });

    $('body').on('submit','#form-appointment',function(e){
      if ($('#form-appointment').valid()) {openModal(e, 'submitBrokerAppointment')}
    });

    $('.choose input[name="quote_type"][value=1]').attr('checked', "checked");
    $('.choose input[type=radio]').change(function(e){
      var value = $(this).val();
      $(this).closest('.step').find('.choose-blocks__item').hide();
      $(this).closest('.step').find('.choose-blocks__item[data-type='+value+']').fadeIn(800);
      if ($("input[name='quote_type'][value=3]:checked").val()) {
        $("button[quote-send='Send to Client']").addClass('disable');
        //$('.js-open-sendclient').css({"pointer-events": "none", "color": "#9c9393"})
        $('.js-open-sendclient').addClass('disable');
      }
      else {
        $("button[quote-send='Send to Client']").removeClass('disable');
        //$('.js-open-sendclient').css({'pointer-events': 'auto', "color": "#6a6a6a"})
        $('.js-open-sendclient').removeClass('disable');
      }
      if ($(this).closest('.step').find('.choose-blocks__item[data-type='+value+']').hasClass('js-end-steps')){
        $(this).closest('.step').find('.js-next-step').hide();
        $(this).closest('.step').nextAll().fadeOut(300).removeClass('done');
      }
      else{
        $(this).closest('.step').find('.js-next-step').fadeIn(300);
      }
    });

    $('.upload input[type=file]').change(function(e){
      if ($(this).val()!=''){
        // uploading file can't be more than 10Mb
        var divUpload = $(this).closest('div.upload');    
        divUpload.find('.upload__size-error').remove();    
        if ($(this).valid()) {          
          divUpload.addClass('done');
          $('#uploadPdf').arcticmodal();
          setTimeout(function() {$('#uploadPdf .modal__close').click()}, 1000)
        } else {
          var labelError = $(this).closest('.upload__input').find('label.error');
          if ( labelError.length ) {
            divUpload.find('label.upload__input').after('<div class="upload__size-error">' + labelError.text() + '</div>');
            divUpload.removeClass('done');
          }
        }  
      } else {
        $(this).closest('.upload').removeClass('done');
      }
      $(this).valid();
    });

    datetimepickerInit();
    teamsInit();
  }

  //FRONTEND


  //functions initialization

  function teamsInit() {
    $.ajax({
      type: "GET",
      url: constants.apiGetTeams
    })
    .done(function(res) {
      for ( var key in res ) {
        var newTeamElem = $('div#team_example').clone();
        var $newTeamElem = $(newTeamElem);

        $newTeamElem.removeAttr('id');
        $newTeamElem.attr('data-cat', res[key].categoryName);
        $newTeamElem.find('.team__item-img img').attr('src', 'assets/images/' + res[key].image).removeClass('logo');
        if(res[key].image === 'logo.svg') {
          $newTeamElem.find('.team__item-img img').addClass('logo');
        }
        $newTeamElem.find('.team__item-img .js-contact-email a').attr('href', 'mailto:' + res[key].email + '?subject=' + res[key].subject).html(res[key].email);
        $newTeamElem.find('.team__item-img .js-contact-phone a').attr('href', 'tel:' + res[key].phone).html(res[key].phone);
        $newTeamElem.find('.team__item-ttl').html(res[key].firstName + ' ' + res[key].lastName);
        $newTeamElem.find('.team__item-position-post').html(res[key].positionPost);
        $newTeamElem.find('.team__item-position').html(res[key].position);
        $newTeamElem.find('.team__item-contact a').attr('href', 'mailto:'+res[key].email+'?subject='+res[key].subject);

        $('#team_list').append(newTeamElem);
      }
    })
    .fail(function() {
    })
    .always(function(){
    });
  }

  function toggleMenu(){
      $('#menu').slideToggle(300).toggleClass('active');
  }

  function toggleBlock(e,target){
    e.preventDefault();
    $('.js-block-'+target).slideToggle(300, function() {
      if (target == 'team' && $('#team').css("display") == "block") {
        $('html, body').animate({        
          scrollTop: $('.link.js-toggle-team').offset().top - $('.header_fixed_clone').outerHeight() - 30
        }, 500);
      };
  
      if (target == 'team' && $('#team').css("display") == "none") {
        $('html, body').animate({
          scrollTop: $('#see').offset().top - $('.header_fixed_clone').outerHeight()
        }, 500);
      };
    });
  }

  function animateTo(target){
    var headerTop = (target.offset().top > h) ? $('header').outerHeight() : 0;
    $('html, body').animate({
      scrollTop: target.offset().top - headerTop
    }, 1000);
  }

  function nextStep($this){
    var cur = $this.closest('.step');
    var next = $this.closest('.step').next();
    var valid = true;
    if ($this.attr('id') == 'btnStep1' && $('#states').val() == $('#states').find("option:first-child").val()) {
      $('#selectState').arcticmodal();
    } else if (!$('input[name="industry"][type="radio"]:checked').length) {
      $('#selectIndustry2').arcticmodal();
    } else if ($this.attr('id') == 'btnStep2' && $('.choose-blocks__item[data-type="3"]').is(":visible") && !$('input[name="custom_rate"][type="checkbox"]:checked').length) {
      $('#selectProducts').arcticmodal();
    }
    else {

      cur.find('input[required]').each(function(){
      valid = (valid && $this.valid());
    });
  
    if (valid){
      if($this.attr('id') == 'btnStep1') {
        var optionsIndustries = '<option class="disabled">What is Your Client\'s Speciality?</option>';
        $.ajax({
          type: "GET",
          url: constants.apiGetCobsByState + $('#states').val()
        })
          .done(function(res) {
            for ( var key in res ) {
              if(res[key].name !== '') {
                optionsIndustries += '<option value="' + res[key].name + '">' + res[key].displayname + '</option>';
              }
            }
          })
          .fail(function() {
          })
          .always(function(){
            $('#industries').html(optionsIndustries);
            $('#industries').prop('selectedIndex',0)
              .trigger('refresh');
          });
      }

      cur.removeClass('current').addClass('done');
      next.fadeIn(300).addClass('current');
      animateTo(next);
    }

    }
  }

  function datetimepickerInit(){
    var defaultDate = new Date();
    var timePickerValue = new Date();
    var hours = timePickerValue.getHours();
    var maxTime = 18;
    var minTime = 9;
    var mins = timePickerValue.getMinutes();
    var quarterHours = Math.round(mins/15);
    var rounded = (quarterHours*15)%60;
    timePickerValue.setHours(hours + 2);
    if (quarterHours == 4)
    {
      timePickerValue.setHours(hours + 3);
    }
    timePickerValue.setMinutes(rounded);

    if (defaultDate.getHours() + 2 >= maxTime) {
      do {
        defaultDate.setDate(defaultDate.getDate() + 1);
      } while (defaultDate.getDay() === 6 || defaultDate.getDay() === 0);
      timePickerValue.setHours(minTime, 0);
    }


    var timeZones = {
      "Pacific": -3,
      "Mountain": -2,
      "Central": -1,
      "Eastern": 0,
    };
    var currentZone = 0;

    var dateTime = {
      year: defaultDate.getFullYear(),
      month: defaultDate.getMonth(),
      date: defaultDate.getDate(),
      hours: timePickerValue.getHours(),
      minutes: timePickerValue.getMinutes(),
    }

    pickmeup('.datepicker', {
      format: 'b d Y',
      position: 'bottom',
      date: defaultDate,
      hide_on_select: true,
      first_day: 0,
      render: function (date) {
        var isPast = false;
        if (date.setHours(0, 0, 0, 0) < defaultDate.setHours(0, 0, 0, 0)) {
          isPast = true;
        }
        if (date.getDay() === 6 || date.getDay() === 0 || isPast) {
          return {disabled : true};
        }
        return {};
      }
    });

    $('.datepicker').on('pickmeup-change', function (e) {
      var selectedDate = e.detail.date;
      dateTime.year = selectedDate.getFullYear();
      dateTime.month = selectedDate.getMonth();
      dateTime.date = selectedDate.getDate();
    });

    var minTimeString = `${timePickerValue.getHours()}:${timePickerValue.getMinutes()}`;
    $("#dtBox").DateTimePicker({
      mode: "time",
      titleContentTime: '',
      defaultDate: timePickerValue,
      timeFormat: "hh:mm AA",
      maxTime: `${maxTime}:00`,
      minTime: minTimeString,
      minuteInterval: 15,
      setValueInTextboxOnEveryClick : true,
      isPopup: false,
      init: function() {
        var oDTP = this;
        oDTP.setDateTimeStringInInputField($('#timePicker input'), timePickerValue);
      },

      afterHide: function(e) {
        var data = this.oData;
        timePickerValue = data.dCurrentDate;
        dateTime.hours = data.iCurrentHour;
        dateTime.minutes = data.iCurrentMinutes;
      },

      addEventHandlers: function() {
        var oDTP = this;
        $('.datepicker').on('pickmeup-change', function (e) {
          var selectedDate = e.detail.date.setHours(0, 0, 0, 0);
          var today = new Date();
          var minimumTime = new Date();// today minimum time
          minimumTime.setHours(today.getHours() + 2 + timeZones[currentZone], today.getMinutes(), 0, 0);
          var selectedTime = new Date();
          selectedTime.setHours(dateTime.hours, dateTime.minutes, 0, 0);

          if (selectedDate === today.setHours(0, 0, 0, 0) && selectedTime < minimumTime) {
            // if today and saved time before 2 hours from now
            // then change input value and minTime
            timePickerValue = minimumTime;
            var minTimeString = `${timePickerValue.getHours()}:${timePickerValue.getMinutes()}`;
            oDTP.settings.minTime = minTimeString;
            oDTP.settings.defaultDate = timePickerValue;
            oDTP.setDateTimeStringInInputField($('#timePicker input'), timePickerValue);
            dateTime.hours = minimumTime.getHours();
            dateTime.minutes = minimumTime.getMinutes();
          } else {
            // if it's not today
            oDTP.settings.minTime = `${minTime}:00`;
            if (dateTime.hours < minTime || dateTime.hours > maxTime ||
              (dateTime.hours === maxTime && dateTime.minutes > 0)) {
              timePickerValue.setHours(minTime, 0);
              oDTP.setDateTimeStringInInputField($('#timePicker input'), timePickerValue);
              dateTime.hours = timePickerValue.getHours();
              dateTime.minutes = timePickerValue.getMinutes();
            }
          }
        });

        $('#sheduletimezone').change(function() {
          const hoursDifference = Math.abs(currentZone) + timeZones[this.value];
          currentZone = timeZones[this.value];
          minTime += hoursDifference;
          maxTime += hoursDifference;
          dateTime.hours += hoursDifference;
          var minimum = oDTP.settings.minTime.split(':');
          var minHour = parseInt(minimum[0]) + hoursDifference;
          oDTP.settings.minTime = `${minHour}:${minimum[1]}`;
          oDTP.settings.maxTime = `${maxTime}:00`;
          timePickerValue.setHours(timePickerValue.getHours() + hoursDifference);
          oDTP.setDateTimeStringInInputField($('#timePicker input'), timePickerValue);
        });
      },
    });
  }

  function pulseGo() {
    $('.btn--circle').addClass('scale--btn')
    setTimeout(function() {$('.btn--circle').removeClass('scale--btn')}, 500),
    $('.video-play').addClass('scale--video'),
    setTimeout(function() {$('.video-play').removeClass('scale--video')}, 700),
      'slow',
    function() {
      $('.btn--circle').addClass('scale--btn')
      setTimeout(function() {$('.btn--circle').removeClass('scale--btn')}, 500),
      $('.video-play').addClass('scale--video'),
      setTimeout(function() {$('.video-play').removeClass('scale--video')}, 700),
        'slow',
        function() {
          start();
        };
    };
  }

})(jQuery);

// var onSubmit = function() {
//   grecaptcha.execute();
//   console.log('Captcha');
// }
