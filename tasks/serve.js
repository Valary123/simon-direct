'use strict';

module.exports = function(options) {

  return function() {

    $.browserSync.init({
      server: options.src,
      browser: 'chrome.exe',
      open: true,
      host: '0.0.0.0'
    });
  };
};
